from django.apps import AppConfig


class TestUtilConfig(AppConfig):
    name = 'test_util'
